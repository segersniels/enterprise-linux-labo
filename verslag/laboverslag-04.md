## Laboverslag: Troubleshooting opdracht 04

- Naam cursist: Niels Segers
- Bitbucket repo: https://bitbucket.org/segersniels/

### Procedures

1.	Controleren of de interface weldegelijk aanligt of problemen ondervind aan de hand van het commando 'ip a'
2.	De interface blijkt offline te liggen. We leggen deze aan door het bestand /etc/sysconfig/network-scripts/ifcfg-enp0s8 aan te passen en de onboot aan te passen naar yes.
3.	Deze krijgt nu het correcte IP toegekend.
4.	We gaan controleren of de service draait, deze draait niet (error).
4. 	De volgende stap is het controleren van de forward lookup file /var/named/cynalco.com
5.	Dit bestand bevat een hoop fouten.
6.	De DNS van tamatama ontbreekt hier. Deze voegen we toe als 'IN   NS    tamatama.cynalco.com.'
7.	Wat ook opvalt is dat butterfree & beedle hun ip's fout staan. Wissel deze 12 & 13 om.
8. 	Het laatste wat aangepast dient te worden is het feit dat Mankey geen alias heeft. Deze voegen we toe: 'files   IN   A      mankey'
9.	Nadat deze file is aangepast gaan we kijken naar onze reverse lookup files 2.0.192 & 56.168.192. Wat opvalt is dat de bestandsnaam van de 192.168 hier al niet klopt.
10.	We passen deze bestandsnaam aan en passen de ORIGIN hier ook aan in dit bestand.
11. Wat opvalt in dit bestand is dat bij 'beedle' en 'butterfree' de 12 en 13 omgewisseld zijn.
12.	Hierna gaan we kijken naar het bestand /etc/named.conf en ondervinden dat hier de listen-on port 53 op 'any;'' moet worden gezet.
13.	We passen hier alle reverse IP's aan naar de correcte IP's. (Ze staan niet reverse)
14. Hierna kijken we alle bestanden nog eens na en ondervinden we dat er bepaalde 'dots' (puntjes) ontbreken achter domeinnamen (/var/named/2.0.192....)
15.	Nadat dit allemaal aangepast is controleren we of we nu de service kunnen starten. Dit werkt nu.
16. Als we nu via ssh connecteren via onze host 'ssh vagrant@192.168.56.42' en het testbestand runnen (golbats) zien we dat we geen errors hebben.
17. Wat ons nu nog rest is om te controleren via een 'dig' command of we via onze host kunnen contacteren met de DNS via het commando 'dig tamatama @192.168.56.42'
18. Dit werkt niet. Dit duid waarschijnlijk op een fout binnen de firewall instellingen. We voegen de service toe aan de firewall met het commando 'sudo firewall-cmd --add-service=dns'
19. We controleren terug aan de hand van een dig commando of we kunnen connecten. Dit werkt nu zonder problemen.

### Testplan en -rapport

- Controleer ofdat de service draait
	- sudo systemctl status named.service -> Deze draait.
- Controleer ofdat de interface enp0s8 aan ligt
	- ip a -> Deze ligt aan.
- Connecteer via ssh met je host en run het test (bats) bestand, dit geeft geen fouten weer.
	- ssh vagrant@192.168.56.42 -> ./golbats_test.bats -> 0 failures
- Wanneer je van op je host probeert te connecteren via 'dig tamatama @192.168.56.42' naar de host volgen er geen problemen.

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Alles ging vrij vlot en zorgde ervoor dat ik al eerste klaar was. 

#### Wat ging niet goed?

- Het was even zoeken waarom mijn named.service niet wou starten omdat ik een 'punt' vergeten plaatsen was in een bestand.

#### Wat heb je geleerd?

- DNS opzetten via BIND en op een efficiente manier in teamverband (Abdulkadir) zo snel mogelijk een oplossing zien te vinden.

#### Waar heb je nog problemen mee?

/

### Referenties

/
