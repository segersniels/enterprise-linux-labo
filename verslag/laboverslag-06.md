## Laboverslag: DHCP opdracht 06

- Naam cursist: Niels Segers
- Bitbucket repo: https://bitbucket.org/segersniels/

### Procedures

1.	Info opzoeken ansible skeleton, tasks & dhcp (eigen vagrant script projecten 02 vorig jaar)
2.  Basic tasks schrijven voor het installeren van de dhcp service
3.  Interface configuratie van de interface enp0s8 moet de DNS server aangepast worden.
4.  Informatie opzoeken over het ansible 'lineinfile' command om dit te verwezenlijken en de server provisionen met nieuwe bevindingen.
5.  Kijken op de server of weldegelijk het config file aangepast is. -> Correct aangepast.
6.  Informate opzoeken over j2 config templates in samenwerking met ansible.
7.  Volgens het script dat ik vorig jaar opgesteld had voor Projecten 02 vorig jaar, het configuratie bestand opgesteld.
8.  De parameters toegekend via het pr001.yml playbook en de server geprovisioned.
9.  Kijken of weldegelijk op de server het configuratie bestand 'dhcpd.conf' aangepast is. -> Correct aangepast
10. Uitzoeken hoe ik via een mac address een fixed IP zou kunnen instellen.
11. Config template aanpassen volgens bevindingen, provisionen op de server en terug bekijken of dit correct aangepast is. -> Correct
12. Wat kleine best practices toegepast.
13. Manueel een minimal centos7 VM draaien om te kijken of deze weldegelijk het juiste IP doorkrijgen.
14. Bij instellingen het juiste host-only network geselecteerd voor beide interfaces en mac address wegschrijven naar het config template script.
15. Server provisionen voor nieuwe settings door te voeren.
16. Interface 1 krijgt een ip uit de gekozen IP pool en interface 2 krijgt het fixed IP 172.16.10.11 toegekend.
17. Overige bestanden van eigen ansible role aangepast (README, CHANGELOG, etc.)

### Testplan en -rapport

- Controleer ofdat de interface enp0s8 aan ligt
	- ip a -> Deze ligt aan.
- Controleer ofdat de configuratie van de interface correct is ingesteld (DNS moet 192.0.2.10 zijn)
	- nano /etc/sysconfig/network-scripts/ifcfg-enp0s8
	- ![enp0s8](https://dl.dropboxusercontent.com/u/63835854/designs/ifcfg-enp0s8.png)
- Controleer het configuratie bestand van dhcpd.conf en controleer of alles correct is ingesteld volgens de opgave.
	- ![dhcpd.conf](https://dl.dropboxusercontent.com/u/63835854/designs/dhcpd.png)
- Bij het aanmaken van een nieuwe (manuele VM) met 2 host-only netwerken moeten deze correcte IP's toegekend krijgen.
  	- ![enp0s3](https://dl.dropboxusercontent.com/u/63835854/designs/enp0s3.png)
  	- ![enp0s8](https://dl.dropboxusercontent.com/u/63835854/designs/enp0s8.png)

- Connecteer via je hostsysteem met smb://brokensamba -> Dit werkt zonder problemen.

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Niet echt veel problemen. 

#### Wat ging niet goed?

- Wat problemen bij het toekennen van het fixed IP om de lijst objecten / variabelen goed te kunnen doorgeven. (undefined)

#### Wat heb je geleerd?

- Volledig eigen werkende ansible role aanmaken
- Leren werken met de .j2 config files en hoe ik deze moet toekennen aan de server
- Variabelen toekennen aan de config files

#### Waar heb je nog problemen mee?

/

### Referenties

/
