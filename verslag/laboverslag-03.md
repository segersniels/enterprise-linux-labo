## Laboverslag taak 3: DNS met BIND

- Naam cursist: Niels Segers
- Bitbucket repo: https://bitbucket.org/segersniels/

### Procedures

####Master

1. De nieuwe vm pu001 werd toegevoegd aan vagrant_hosts.yml en ansible/site.yml. 
2. De role bertvv.bind bertvv.el7 werden geinstalleerd via ansible galaxy en toegekend aan de juiste yml files.
4. pu001.yml werd aangepast volgens het netwerk dat te vinden is in de README.md.
5. De map filter_plugins in de role bertvv.bind werd gekopieerd naar ${ANSIBLE_HOME}. 

####Slave

1. De nieuwe vm pu002 werd toegevoegd aan vagrant_hosts.yml en ansible/site.yml.
2. De role bertvv.bind bertvv.el7 werden geinstalleerd via ansible galaxy en toegekend aan de juiste yml files.
4. pu002.yml werd aangepast volgens het netwerk,(zonder `bind_zone_hosts`).
5. `el7_firewall_allow_services: dns` toevoegen aan group_vars/all.yml zodat dns door de firewall kan.


### Testplan en -rapport

1. Voer vagrant status uit. Je zou 3 VMs moeten zien met namen pu001, pu002 en pu004 en status not created. Als deze toch bestaat, doe dan eerst `vagrant destroy -f pu001` en `vagrant destroy -f pu002`.
2. Voer `vagrant up pu001` en `vagrant up pu002` uit. De commando's moeten slagen zonder fouten (exitstatus 0)
3. Log in op de servers met `vagrant ssh "servernaam"` en voer de acceptatietests uit: `sudo /vagrant/test/runbats.sh`. Alle tests moeten slagen.
4. De service moet beschikbaar zijn vanaf het hostsysteem: `dig linuxlab.lan any 192.0.2.10` en `dig linuxlab.lan any 192.0.2.11`

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

Alles ging vrij vlot.

#### Wat ging niet goed?

Ik kreeg een hele hoop fouten bij de acceptatietests voor de Slave DNS server 8 van de 14 gaven een error. Dit kwam grotendeels doordat ik de dns service niet had toegevoegd aan de firewall van de server.

#### Wat heb je geleerd?

In combinatie met opdracht 4 heb ik ondervonden hoe een DNS server te installeren via BIND en hoe de configuratie bestanden in zen werk gaan.

#### Waar heb je nog problemen mee?

el7 error opeens uit het niets.

### Referenties

https://github.com/bertvv/ansible-role-bind

