## Laboverslag: Troubleshooting opdracht 04

- Naam cursist: Niels Segers
- Bitbucket repo: https://bitbucket.org/segersniels/

### Procedures

1.	Controleren wat er fout is in /etc/samba/smb.conf
2.  IP staat correct ingesteld ('ip a')
2.  Workgroup staat fout ingesteld -> veranderd naar LINUXLAB
3.  sudo systemctl start nbm.service
4.  Netbios error is nu opgelost
5.  /etc/samba/smb.conf shares aanpassen volgens de opdracht
6.  'writable' aanpassen waar nodig (bijvoorbeeld charlie writable = no)
7.  ownership aangepast van echo en alpha (sudo chown -R ...)
8.  valid users aangepast bij de nodige shares (alpha & echo)
9.  'sudo setsebool -P samba_export_all_rw 1' lost bravo error op (geeft samba access tot alles)

### Testplan en -rapport

- Controleer ofdat de interface enp0s8 aan ligt
	- ip a -> Deze ligt aan.
- Controleer ofdat de services draaien
	- sudo systemctl status smb.service & nmb.service -> Deze draaien.
- Run het test script ./runbats.sh -> Dit geeft geen errors

  ![screenshot](Troubleshooting2.png)

- Connecteer via je hostsysteem met smb://brokensamba -> Dit werkt zonder problemen.

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Geen problemen, alles verliep vlot. 

#### Wat ging niet goed?

- Het was even zoeken om de error van Bravo te doen werken en op te lossen.

#### Wat heb je geleerd?

- Samba shares troubleshooting op de machine zelf.

#### Waar heb je nog problemen mee?

/

### Referenties

/
