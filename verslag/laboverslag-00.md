## Laboverslag: opdracht00

- Naam cursist: Niels Segers
- Bitbucket repo: https://bitbucket.org/segersniels/

### Procedures

1. BitBucket account aangemaakt.
2. Fork gemaakt van Bert Van Vreckem repo
3. git clone
4. Sjablonen en bestanden aangepast naar eigen naam / repo.
5. git add . / git commit -m '' / git push
6. Ansible setup & init
7. Via ansible-galaxy de role geinstalleerd / gedownload
8. Ansible all.yml file aangepast om overeenkomende repository el7 te installeren
9. vagrant provision om alle nodige variabelen & settings te pushen naar de virtuele machine.
10. Tests runnen
11. Verslag aanvullen en pushen naar GIT met alle andere updates.

### Testplan en -rapport

####Testplan

0. Ga op het hostsysteem naar de directory met de lokale kopie van de repository. 

1. Voer vagrant status uit
	• je zou een VM moeten zien met naam pu004 en status not created. Als deze toch bestaat, doe dan eerst vagrant destroy -f pu004.

2. Voer vagrant up pu004 uit.
	• Het commando moet slagen zonder fouten (exitstatus 0)

3. Log in op de server met vagrant ssh srv010 en voer de acceptatietests uit:
``` shell
     [vagrant@pu004 test]$ sudo /vagrant/test/runbats.sh
     Running test /vagrant/test/common.bats
     ✓ EPEL repository should be available
     ✓ Bash-completion should have been installed
     ✓ bind-utils should have been installed
	 ✓ Git should have been installed
   	 ✓ Nano should have been installed
     ✓ Tree should have been installed
     ✓ Vim-enhanced should have been installed
     ✓ Wget should have been installed
     ✓ Admin user bert should exist
     ✓ Custom /etc/motd should be installed
     10 tests, 0 failures
```

4. Log uit en log vanop het hostsysteem opnieuw in, maar nu met ssh. Er mag geen wachtwoord gevraagd worden.
``` shell
	$ ssh bert@192.0.2.50
	Welcome to pu004.localdomain.
	enp0s3     : 10.0.2.15
	enp0s8     : 192.0.2.50
	[bert@pu004 ~]$
```
####Rapport

0. cd enterprise-linux-labo
1. VM werd verwijderd met vagrant halt & vagrant destroy na het zien van vagrant status
2. VM werd opnieuw aangemaakt met vagrant up
3. Acceptatietest geeft weer:
``` shell
     [niels@pu004 ~]$ sudo /vagrant/test/runbats.sh 
     Running test /vagrant/test/common.bats
     ✓ EPEL repository should be available
     ✓ Bash-completion should have been installed
     ✓ bind-utils should have been installed
     ✓ Git should have been installed
     ✓ Nano should have been installed
     ✓ Tree should have been installed
     ✓ Vim-enhanced should have been installed
     ✓ Wget should have been installed
     ✓ Admin user niels should exist
     ✓ Custom /etc/motd should have been installed
```

4. inloggen met ssh niels@192.0.2.50 gaat zoals het hoort en vraagt geen wachtwoord.

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Alles ging vrij vlot aangezien ik al een GIT werkomgeving opgesteld had staan voor eigen gebruik.
- Vagrant verliep vlot
- Ansible werkt zoals het hoort, mits wat moeilijkheden onderweg

#### Wat ging niet goed?

- Ansible zorgde wel wat voor problemen aangezien ik hier nog niet mee gewerkt had
- Ik was begonnen met een eigen playbook te schrijven waardoor ik veel problemen ondervond
- Uiteindelijk met de el7 role verder gewerkt en ging alles beter

#### Wat heb je geleerd?

- Ansible playbook configuration

#### Waar heb je nog problemen mee?

- /

### Referenties

-Chamilo opdrachtomschrijving voor richtlijnen naar URL van bitbucket, etc. 
