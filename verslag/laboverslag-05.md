## Laboverslag:  Opdracht 05

- Naam cursist: Niels Segers
- Bitbucket repo: https://bitbucket.org/segersniels/

### Procedures

1.	Informatie opgezocht over Samba & vsftp
2.	De bertvv.samba role via ansible-galaxy gedownload
3.	De benodigde instellingen aangepast en toegevoegd aan de Vagrant bestanden voor de nieuwe server op te zetten
4.	Na het opzetten van de nieuwe server de benodigde instellingen (shares, users, etc.) toegevoegd aan het pr011.yml playbook
5. 	De basic instellingen opgesteld in het site.yml playbook el7, etc.
6.	Na het opzetten van alle samba instellingen de server geprovisioned
7.	Connecten van host naar client werkt via Samba zonder problemen.
8.	Informatie opzoeken over vsftp en ansible galaxy playbooks
9.	Ansible skeleton gedownload en beginnen aanpassen waar nodig voor een nieuwe role aan te maken
10. Playbooks beginnen opstellen voor het downloaden, installeren en instellen van de vsftp server
11.	Via gewone bash issues de config files aanpassen voorlopig
12. Server provision werkt zonder problemen
13. Probleem ontdekt bij 'lineinfile' command van ansible dat het maar de laatste line update en niet de volledige lijst van items
14. Tests geven veel errors terug door het niet werken van de 'lineinfile' loop
15. Role in samenwerking met collega's verder bekeken en deze role gebruikt
16. Nodige yml files aangepast waar nodig
17. Connecten via ftp werkt zonder problemen nu en tests geven geen errors.

### Testplan en -rapport

- Controleer ofdat de services draaien
	- sudo systemctl status samba.service -> Deze draait.
	- sudo systemctl status vsftpd.service -> Deze draait.
- Controleer ofdat de interface enp0s8 aan ligt
	- ip a -> Deze ligt aan.
- Connecteer via ssh en run het test (bats) bestand, dit geeft geen fouten weer.
	- ssh vagrant@172.16.0.11-> ./golbats_test.bats -> 0 failures
- Connecten van host via samba & ftp zouden geen problemen mogen geven voor de correcte gebruikers.
- smb://files - ftp://files

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Het aanmaken van de ansible role verliep zeer stroef.

#### Wat ging niet goed?

- Het doorlopen van de 'with_items' werkte niet met het 'lineinfile' command van ansible. Dus overgestapt op een andere role.

#### Wat heb je geleerd?

- Aan de hand van een ansible skeleton een playbook aanmaken en een server provisionen.

#### Waar heb je nog problemen mee?

- 'lineinfile'

### Referenties

/
