## Laboverslag: Opdracht 01 Actualiteit: Eigen Ansible role schrijven

- Naam cursist: Niels Segers
- Bitbucket repo: https://bitbucket.org/segersniels/
- Cheatsheet link: https://bitbucket.org/segersniels/enterprise-linux-labo/src/fd465ce22329425460db58d0bd6b5760898a2d00/doc/cheat-sheet.md?at=master&fileviewer=file-view-default

### Cheat sheet

- Naargelang ik de oprachten voor dit vak in orde bracht en tot een goed einde probeerde te brengen, vulde ik alle commando's aan die ik regelmatig nodig had doorheen het OLOD.
De cheatsheet zelf is niet meteen zeer uitgebreid, omdat ik de meeste basis commando's al in mijn vingers had zitten. Maar toch heb ik geprobeerd om toch een duidelijk overzicht te geven gaande van basic commando's in linux tot een console oriented git werkomgeving tot het installeren van ansible roles.

- Ook probeerde ik zeer kort enkele handige file locations en folders weer te geven waar het meeste van de opdrachten werk zich bevond. Door het gebruiken van roles gemaakt door andere mensen is het niet altijd meteen duidelijk waar de bestanden/configuratie bestanden zich bevinden op het systeem zelf, dus is het handig om een klein overzichtje te hebben.

- Een checklist voor enkele kleine zaken is ook beschikbaar, deze is jammer genoeg niet uitgebreid en verwijst eerder naar het kijken van de configuratie bestanden.

### Bijdrage aan een open source project

- Het aanmaken van een eigen ansible role voor het opstellen van een DHCP server.
	Vorig academiejaar kregen we al eerder de opdracht om een DHCP server op te stellen aan de hand van Vagrant.
	Omdat ik zeer geinteresseerd was vorig academiejaar door de automatisatie via Vagrant en ik dus ook alle Linux servers opgesteld had voor mijn groep, leek Ansible mij daarom zeker interessant om mee te leren werken. Ik had al gehoor van Cheff & Ansible maar nooit eerder echt actief naar gekeken om deze te gebruiken.
	Dus voor mezelf wat te oefenen nam ik het shellscript dat ik vorig jaar gebruikt had en heb ik dit omgevormd naar een ansible role met een aangepaste config template om alles zo geautomatiseerd mogelijk te laten verlopen. Waarbij user input welkom is en niet meer zoals bij Vagrant alles hardcoded was in mijn Shell script.

	+ Tijdens het aanmaken van deze DHCP role heb ik ook nog even snel mijn vsftp role herbekeken en aangepast waar ik kon (kleine foutjes). Deze werkt nu ook volledig in samenwerking met samba en de tests slagen. Deze stelt gewoon hardcoded config settings door via 'lineinfile'.

### Hoe?

- Ansible skeleton gedownload
- Informatie opgezocht hoe te werken met het skeleton, waar wat hoort te komen, mappenstructuur.
- Gekeken naar de roles die al gebruikt werden voor de vorige opdrachten. Uitgezocht welke commando's werden gebruikt, hoe de variabelen werden doorgegeven en gebruikt konden worden.
- Research naar interessante commando's zoals 'lineinfile' om wat basic tasks in orde te steken en de config files in orde ge brengen.
- Al snel bleek duidelijk dat dit niet de gewenste manier is om zoiets te verwezenlijken (best practices). -> Meerdere variabelen meegeven in een enkele lineinfile ging wat moeilijk.
- Beginnen informatie opzoeken over het werken met configuratie bestanden / templates in Ansible.
- Template dhcpd.conf.j2 aangepast met de correcte layout volgens het beschikbare script (https://github.com/segersniels/vagrant-project2/blob/master/DHCP/bootstrap.sh)
- Zaken toegevoegd aan de role die nog niet waren verwerkt in het script (fixed addresses/mac)
- Tasks -> main.yml aangevuld met tasks en referenties voor het .j2 config bestand
- Role overgeplaatst in mijn eigen segersniels repository en toegevoegd aan ansible (https://galaxy.ansible.com/detail#/role/6402)

### Testplan en -rapport

####Testplan

- ansible-galaxy install segersniels.dhcp
- Wanneer deze geinstalleerd is ga je naar je playbook van je server waar je deze role op wilt toepassen.
- Vul de nodige parameters in die je kan vinden in de README.md van de ansible role
- Wanneer alles correct verloopt en je kan provisionen zonder problemen, orienteer je naar /etc/dhcp/dhcpd.conf en bekjk je de inhoud.
- Hier kijk je nog eens na of alles wel klopt zoals het hoort.

	- ![dhcpd.conf](https://dl.dropboxusercontent.com/u/63835854/designs/dhcpd.png)

- Om te testen of de dhcp server correct werkt, maak je manueel een nieuwe machine aan op het zelfde host-only netwerk van je dhcp server.
- Bij deze machine laat je het ip toekennen via dhcp en zou een IP moeten toegekend krijgen in de range die je zelf gedefenieerd hebt.
- Om een ip te reserveren binnen je gekozen (private) range kijk je naar de README.md voor de nodige parameters en maak je in het playbook van je server een reservation aan.
- Provision de server en controleer de andere machines of alles werkt zoals het hoort.

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Alles ging vlot, tot mijn eigen verbazing. Het hielp natuurlijk wel dat ik wat kennis had van vorig jaar met de DHCP vagrant (shell script) opstelling.

#### Wat ging niet goed?

- Enkele problemen met dhcp toekenning in het midden van de ip range, ondertussen opgelost.
- Regelmatig vergeten git add / git push doen van de cheatsheet, wat ervoor zorgde dat er enkel in grote stukken geupdate is geweest aan de cheatsheet.

#### Wat heb je geleerd?

- Ansible role setup
- Config file opstellingen

#### Waar heb je nog problemen mee?

- /

### Referenties

- https://github.com/segersniels/vagrant-project2/blob/master/DHCP/bootstrap.sh
- https://github.com/bertvv/ansible-role-skeleton
- http://docs.ansible.com/ansible/lineinfile_module.html
- http://docs.ansible.com/ansible/template_module.html
- https://galaxy.ansible.com/detail#/role/6402