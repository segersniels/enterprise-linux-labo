## Laboverslag: opdracht02

- Naam cursist: Niels Segers
- Bitbucket repo: https://bitbucket.org/segersniels/

### Procedures

1. Ansible documentatie doorlezen
2. Roles geinstalleerd (bertvv.httpd, bertvv.mariadb & bertvv.wordpress) via ansible-galaxy
3. site.yml playbook aangepast volgens de correcte roles
4. Gecontroleerd of vagrant provision werkt zoals het hoort, geen fouten
5. Navigeren naar http://192.0.2.50/wordpress werkt zoals het hoort en toont de installpage.
6. Runnen van de runbats.sh test.

### Testplan en -rapport

####Testplan

0. Het uitvoeren van een vagrant up of vagrant provision verloopt zonder fouten en de server word opgezet.
1. Controleer of de benodigde services draaien (httpd & mariadb)
2. Wanneer er genavigeerd word naar http://192.0.2.50 komt de gebruiker op de install pagina van Wordpress.
3. Inloggen via vagrant ssh & ssh niels@192.0.2.50 logt de gebruiker in op het systeem.
4. Log in op de server met vagrant ssh en voer de acceptatietests (runbats.sh) uit:

``` shell
     [vagrant@pu004 test]$ sudo /vagrant/test/runbats.sh
     Running test /vagrant/test/common.bats
     ✓ EPEL repository should be available
     ✓ Bash-completion should have been installed
     ✓ bind-utils should have been installed
	 ✓ Git should have been installed
   	 ✓ Nano should have been installed
     ✓ Tree should have been installed
     ✓ Vim-enhanced should have been installed
     ✓ Wget should have been installed
     ✓ Admin user niels should exist
     ✓ Custom /etc/motd should be installed
     10 tests, 0 failures
     Running test /vagrant/test/pu004/lamp.bats
      ✓ The necessary packages should be installed
      ✓ The Apache service should be running
      ✓ The Apache service should be started at boot
      ✓ The MariaDB service should be running
      ✓ The MariaDB service should be started at boot
      ✓ The SELinux status should be ‘enforcing’
      - Firewall: interface enp0s8 should be added to the public zone (skipped)
      ✓ Web traffic should pass through the firewall
      ✓ Mariadb should have a database for Wordpress
      ✓ The MariaDB user should have "write access" to the database
      ✓ The website should be accessible through HTTP
      ✓ The website should be accessible through HTTPS
      ✓ The certificate should not be the default one
      ✓ The Wordpress install page should be visible under http://192.0.2.50/wordpress/
      ✓ MariaDB should not have a test database
      ✓ MariaDB should not have anonymous users
     
     16 tests, 1 failure, 1 skipped
```

####Rapport

0. Vagrant up werkt zonder problemen, provisioning verloopt ook zonder fouten
1. Alle benodigde services draaien (systemctl status httpd.service, etc.)
2. Er kan succesvol gesurft worden naar het benodigde IP en er wordt geredirect naar de install page van WordPress
3. Inloggen werkt nog steeds zoals het hoort
4. Acceptatietest geeft weer:
``` shell
     [niels@pu004 vagrant]$ sudo ./test/runbats.sh 
     Running test /vagrant/test/common.bats
      ✓ EPEL repository should be available
      ✓ Bash-completion should have been installed
      ✓ bind-utils should have been installed
      ✓ Git should have been installed
      ✓ Nano should have been installed
      ✓ Tree should have been installed
      ✓ Vim-enhanced should have been installed
      ✓ Wget should have been installed
      ✓ Admin user niels should exist
      ✓ Custom /etc/motd should have been installed
     
     10 tests, 0 failures
     Running test /vagrant/test/pu004/lamp.bats
      ✓ The necessary packages should be installed
      ✓ The Apache service should be running
      ✓ The Apache service should be started at boot
      ✓ The MariaDB service should be running
      ✓ The MariaDB service should be started at boot
      ✓ The SELinux status should be ‘enforcing’
      - Firewall: interface enp0s8 should be added to the public zone (skipped)
      ✓ Web traffic should pass through the firewall
      ✓ Mariadb should have a database for Wordpress
      ✓ The MariaDB user should have "write access" to the database
      ✓ The website should be accessible through HTTP
      ✓ The website should be accessible through HTTPS
      ✗ The certificate should not be the default one
        (in test file test/pu004/lamp.bats, line 95)
          `[ -z "$(echo ${output} | grep SomeOrganization)" ]' failed
      ✓ The Wordpress install page should be visible under http://192.0.2.50/wordpress/
      ✓ MariaDB should not have a test database
      ✓ MariaDB should not have anonymous users
     
     16 tests, 1 failure, 1 skipped
```

### Retrospectieve

- Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Roles toekennen
- Surfen naar http://192.0.2.50 gaat zonder problemen
- LAMP server draait zoals het hoort

#### Wat ging niet goed?

- Ik was eerst begonnen met een eigen playbook op te stellen en mijn eigen lamp stack server op te stellen, maar kwam al snel tot de conclusie dat dit niet zo eenvoudig was en kwam veel problemen tegen. Hierdoor ben ik overgestapt op de bertvv roles manier.

#### Wat heb je geleerd?

- Ansible kan zeer eenvoudig aan de hand van tasks commando's en zaken uitvoeren waarbij je met enkel Vagrant een volledig bash script zou moeten schrijven.

#### Waar heb je nog problemen mee?

- Certificaat nog niet volledig werkend gekregen

### Referenties

- Bert Van Vreckem github repo
- Google
- Eigen github repo vorig jaar (lamp stack)
