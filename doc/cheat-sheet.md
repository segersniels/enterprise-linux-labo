## Cheat sheet en checklists

- Naam student: Niels Segers
- Bitbucket repo: https://bitbucket.org/segersniels/

### Commands

#####Linux

| Taak                   	   					| Commando 		  									|
| :---                   	   					| :---     		  						  			|
| IP-adress(en) opvragen linux 					| `ip addr show`									|
| Databases opvragen van MariaDB 				| `mysql -> SHOW DATABASES;`   						|
| Change ownership of a specific file/directory | `chown 'user:group' 'directory/folder'`  				|
| Change rwx permissions of a file/directory 	| `chmod 775 'directory/folder'` 		    			|
| Add user to linux (lowlevel) 					| `useradd user:group`   							|
| Add a service to the firewall 				| `firewall-cmd --permanent --add-service='service'`	|
| Add user to linux (lowlevel) 					| `useradd 'user:group'`   							|
| Changing SELinux boolean settigns 		    | `setsebool 'boolean' on/off`   					    |
| Get boolean info SELinux 						| `getsebool`   						         	|
| See the permissions of all files in a directory						| `ls -l 'directory'`   						         	|

#####Ansible

| Taak                   	   					| Commando 		  									|
| :---                   	   					| :---     		  						  			|
| Search and edit a line in a file 				| `lineinfile: dest='destination' regexp='regexpression' state='state'`		|
| Run a bash command 				| `command: 'insert bash command here'`		|


#####MacOSX

| Taak                   			| Commando 														 |
| :---                   			| :---     														 |
| IP-adress(en) opvragen MacOSX 	| `ifconfig`  													 |	
| Configuring GIT unixwise username | `git config --global user.name "username"`   					 |
| Configuring GIT unixwise email 	| `git config --global user.email email@example.com`   			 |
| Installing an ansible-galaxy role | `ansible-galaxy -p install ansible/roles 'insert role here'`   |

#####Git workflow

| Taak                                        | Commando                  |
| :---                                        | :---                      |
| Huidige toestand project                    | `git status`              |
| Bestanden toevoegen/klaarzetten voor commit | `git add FILE...`         |
| Bestanden "committen"                       | `git commit -m 'MESSAGE'` |
| Synchroniseren naar Bitbucket               | `git push`                |
| Wijzigingen van Bitbucket halen             | `git pull`                |
| Clonen van een GITHUB repository            | `git clone 'https repo'`                |

#####Vagrant workflow

| Taak                                        | Commando                 	    |
| :---                                        | :---                      		|
| Start vagrant server                   	  | `vagrant up 'servername'`       |
| Provision the running vagrant server        | `vagrant provision 'servername'`|
| Remove the started vagrant server           | `vagrant destroy 'servername'` 	|
| Check the status of the vagrant server      | `vagrant status 'servername`    |

#####.j2 Config file

| Taak                                        | Commando                 	    |
| :---                                        | :---                      		|
| If          	  | `{% if 'insert variable name' is defined %}{% endif %}`       |
| For          	  | `{% for 'single object name of your choice' in 'list of objects' %}{% endfor %}`       |
| Use a variable that is defined in another file    | `{{ 'variable name' }}`       |

### Folders/files

| Taak                                        | Commando                 	    |
| :---                                        | :---                      		|
| Network interface configuration             | `/etc/sysconfig/network-scripts/ifcfg-enp0s8`       |
| Samba config file        					  | `/etc/samba/smb.conf`|
| DHCP config file            				  | `/etc/dhcpd/dhcpd.conf` 	|
| Vsftp config file      					  | `/etc/vsftpd/vsftpd.conf`   |
| Bind config file      					  | `/etc/named.conf`   |

### Checklist netwerkconfiguratie

1. Is het IP-adres correct? `ip a`
2. Is de router/default gateway correct ingesteld? `ip r -n`
3. Is er een DNS-server? `cat /etc/resolv.conf`
4. Is de configuratie van de netwerk interface in orde? `nano /etc/sysconfig/network-scripts/ifcfg-enp0s8`

### Checklist Samba/vsftp

1. Draaien de services?
	- `sudo systemctl status samba.service`
	- `sudo systemctl status vsftpd.service`
2. Ligt de netwerkinterface enp0s8 aan?
	- `ip a` 
3. Kan je connecteren via ssh met je virtuele machine? Geven de testbestanden fouten? `ssh vagrant@172.16.0.11` -> `sudo ./golbats_test.bats`
4. Kan je connecteren via je host naar de samba/vsftp server? -> smb://files - ftp://172.16.0.11
5. Laat de firewall ftp connectie toe? `firewall-cmd --permanent --add-service=ftp`
6. Zijn er geen fouten gekropen in de configuratie van de shares? `nano /etc/samba/smb.conf`

### Checklist DHCP

1. Klopt het eigen address en de DNS server van de DHCP server? `ip a` `nano /etc/sysconfig/network-scripts/ifcfg-enp0s8`
2. Zijn de instellingen van de DHCP server wel in orde? `nano /etc/dhcp/dhcpd.conf` 

### Checklist BIND

1. Kloppen de configuratie instellingen van de bind server? Zijn er geen puntjes vergeten waar nodig? `nano /etc/named.conf`
2. Draait de service? `sudo systemctl status named.service`

