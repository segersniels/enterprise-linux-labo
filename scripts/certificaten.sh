#! /bin/bash
#
# Author: Andy Nelis
# script certificaten.sh

sudo yum install mod_ssl openssl

sudo openssl genrsa -out ca.key 2048 
sudo openssl req -new -key ca.key -out ca.csr	
#(hier moet informatie meegegeven worden)

sudo openssl x509 -req -days 365 -in ca.csr -signkey ca.key -out ca.crt

sudo cp ca.crt /etc/pki/tls/certs
sudo cp ca.key /etc/pki/tls/private/ca.key
sudo cp ca.csr /etc/pki/tls/private/ca.csr

sudo restorecon -RvF /etc/pki

sudo chmod 777 /etc/httpd/conf.d/ssl.conf
sudo vi +/SSLCertificateFile /etc/httpd/conf.d/ssl.conf 
#volgende dingen moeten aangepast worden in ssl.conf:
#SSLCertificateFile /etc/pki/tls/certs/ca.crt
#SSLCertificateKeyFile /etc/pki/tls/private/ca.key

#nu moet vagrant halt uitgevoerd worden en daarna vagrant up
